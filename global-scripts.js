window.addEventListener('load', load_mobile_menu)
window.addEventListener('resize', load_mobile_menu)

async function load_mobile_menu() {
	if (600 >= window.outerWidth) {
		window.removeEventListener('load', load_mobile_menu)
		window.removeEventListener('resize', load_mobile_menu)

		await import(/* webpackChunkName: "mobile-menu-styles" */ 'mmenu-js/src/mmenu.scss')
		const menu_module = await import(/* webpackChunkName: "mobile-menu" */ 'mmenu-js')
		const Mmenu = menu_module.default

		await import(/* webpackChunkName: "mobile-menu-polyfills" */ 'mmenu-js/dist/mmenu.polyfills')

		const menu = new Mmenu('#mobile_menu_js', {
			counters: true,
			extensions: ['pagedim-black', 'position-right', 'shadow-page', 'shadow-panels', 'theme-light'],
		})

		const api = menu.API

		document.getElementById('main_menu_toggle_js').addEventListener('click', event => {
			event.preventDefault()

			api.open()
		})

		document.getElementById('mobile_menu_js').classList.remove('hidden')
	}
}
